#include <stdio.h>
#include <stdlib.h>

struct Struktura
{
	unsigned char *wskchar;
	int zmInt;
};



int main()
{
	struct Struktura zmienna_strukt;
	struct Struktura *wskaznik;

	zmienna_strukt.zmInt = 11;
	wskaznik = &zmienna_strukt;
	printf("Wartosc dla zmiennej2 = %d\n", zmienna_strukt.zmInt);

	zmienna_strukt.wskchar = NULL;
	printf("Wartosc dla zmiennej1 = %s\n", zmienna_strukt.wskchar);

	unsigned char a = 's';
	wskaznik->wskchar = &a;
	wskaznik->zmInt = 24;
	printf("Wartosc dla zmiennej2 = %d\n", zmienna_strukt.zmInt);
	printf("Wartosc dla zmiennej1 = %c\n", *(zmienna_strukt.wskchar));
	return 0;
}
