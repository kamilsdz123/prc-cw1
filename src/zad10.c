#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct struktura1
{
	unsigned char zmienna1;
	unsigned char zmienna2;
	unsigned char zmienna3;
	unsigned char zmienna4;
};



int main(void)
{
	int a = 0x12345678;
	struct struktura1 zmienna;
	memcpy(&zmienna, &a, sizeof(zmienna));
	printf("Zmienna = %x %x %x %x \n", zmienna.zmienna1, zmienna.zmienna2, zmienna.zmienna3, zmienna.zmienna4);
	return 0;
}
