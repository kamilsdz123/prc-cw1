

#include <stdio.h>
#include <stdlib.h>

int main()
	{
		//Deklaracja zmiennych

		int zmiennaint;
		char zmiennachar;
		unsigned char zmiennaunsignedchar;
		float zmiennafloat;
		double zmiennadouble;

		//Przypisanie wartosci zmiennym

		zmiennaint = 5;
		zmiennachar = 'c';  			//zmienna znakowa
		zmiennaunsignedchar = 's';
		zmiennafloat = 17.275166163267147;
		zmiennadouble = 52.8215712681176723;

		//Wyswietlenie zmiennych

		printf("Wartosci zmiennych: \n");
		printf("	int = %d \n",zmiennaint);
		printf("	char = %c \n",zmiennachar);
		printf("	unsigned char = %c \n",zmiennaunsignedchar);
		printf("	float = %5f \n",zmiennafloat);
		printf("	double = %.12f \n \n",zmiennadouble);

		//Rozmiary zmiennych
		printf("Rozmiary zmiennych: \n");
		printf("	Rozmiar zmiennej int: %d \n",sizeof(zmiennaint));
		printf("	Rozmiar zmiennej char: %i \n",sizeof(zmiennachar));
		printf("	Rozmiar zmiennej unsigned char: %i \n",sizeof(zmiennaunsignedchar));
		printf("	Rozmiar zmiennej float: %i \n",sizeof(zmiennafloat));
		printf("	Rozmiar zmiennej double: %i \n",sizeof(zmiennadouble));

		const int XD = 7;
		printf("XD: %d",XD);

		return 0;

	}



