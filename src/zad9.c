#include <stdio.h>
#include <stdlib.h>

struct struktura1
{
	unsigned char p1 :1, p2 :1, p3 :1, p4 :1, p5 :1, p6 :1, p7 :1, p8 :1;
};

struct struktura2
{
	unsigned char p1 :1, p2 :1, p3 :1;
};

struct struktura3
{
	unsigned int p1 :1, p2 :1, p4_4bit :4;
};



int main(void) {

printf("Rozmiar struktury 1 = %ld \n", sizeof(struct struktura1));
printf("Rozmiar struktury 2 = %ld \n", sizeof(struct struktura2));
printf("Rozmiar struktury 3 = %ld \n", sizeof(struct struktura3));
printf("\n");

struct struktura1 jeden;
struct struktura2 dwa;
struct struktura3 trzy;

jeden.p1 = 1;
jeden.p2 = 1;
jeden.p3 = 1;
jeden.p4 = 1;
jeden.p5 = 1;
jeden.p6 = 1;
jeden.p7 = 1;
jeden.p8 = 1;

dwa.p1 = 1;
dwa.p2 = 1;
dwa.p3 = 0;

trzy.p1 = 0;
trzy.p2 = 0;
trzy.p4_4bit = 150;



printf("Struktura 1 = %d%d%d%d%d%d%d%d \n", jeden.p1, jeden.p2, jeden.p3,jeden.p4, jeden.p5, jeden.p6, jeden.p7, jeden.p8);
printf("Struktura 2= %d%d%d\n", dwa.p1, dwa.p2, dwa.p3);
printf("Struktura 3 = %d%d%d\n", trzy.p1, trzy.p2, trzy.p4_4bit);
printf("\n");


printf("Pierwsza zmienna = %d\n", jeden);
printf("Druga zmienna = %d\n", dwa);
printf("Trzecia zmienna = %d\n", trzy);
return 0;


}

