#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "header.h"



void Wyswietlanie (struct bufor *Lista)
{

	int i = 1;
	for (Lista = Lista->pierwszy; Lista != NULL; Lista = Lista->nastepny)
	{
		printf("Element nr %d :  %d,  %d \n", i, Lista->zmienna1, Lista->zmienna2);
		i++;
	}
}

void Usuwanie (struct bufor *Lista, int nr_ele){
	int i=1;
	struct bufor *Pamiec;
	for (Lista = Lista->pierwszy; Lista != NULL; Lista = Lista->nastepny) {

		if (i == nr_ele){
			Lista->poprzedni->nastepny = Lista->nastepny;
			Lista->nastepny->poprzedni = Lista->poprzedni;
			Pamiec = Lista;
		}
		i++;
	}
	free(Pamiec);
}

int main() {

	struct bufor *Lista;
	struct bufor *Pamiec;

	for (int i = 0 ; i<10 ; i++){
		if (i == 0) {
			Lista = (struct bufor*) malloc(sizeof(struct bufor));
			Lista->pierwszy = Lista;
			Lista->nastepny = NULL;
			Lista->poprzedni = NULL;
			Lista->zmienna1 = 5;
			Lista->zmienna2 = 10;
			Pamiec = Lista;
		}
		else {
			Lista = (struct bufor*) malloc(sizeof(struct bufor));
			Pamiec->nastepny = Lista;
			Lista->pierwszy = Pamiec->pierwszy;
			Lista->nastepny = NULL;
			Lista->poprzedni = Pamiec;
			Lista->zmienna1 = i*5+5;
			Lista->zmienna2 = i*10+10;
			Pamiec = Lista;
		}
	}

	Wyswietlanie(Lista);

	Usuwanie(Lista,5);
	Usuwanie(Lista,6);

	printf("\nPo usunieciu elementu 5 i 7\n");

	Wyswietlanie(Lista);



	return (0);
}

