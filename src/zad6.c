#include <stdio.h>
#include <stdlib.h>

int main(void) {
	unsigned int *wsk_do_bufora;
	unsigned int liczba_elementow_bufora = 7;
	wsk_do_bufora = (unsigned int*) malloc(liczba_elementow_bufora * sizeof(unsigned int));
	for (int i = 0; i < liczba_elementow_bufora; i++) {
		wsk_do_bufora[i] = i + 100;

	}

	printf(
			"rozmiar wsk = %ld bajtow oraz rozmiar zmiennej liczba elementow bufora = %ld bajtow \n",
			sizeof(wsk_do_bufora), sizeof(liczba_elementow_bufora));

	for (int i = 0; i < liczba_elementow_bufora; i++) {

		printf("element numer %d = %d\n", i, wsk_do_bufora[i]);
	}

	printf("-----------------------\n");

	for (int i = 0; i < liczba_elementow_bufora; i++) {
		wsk_do_bufora[i] = 0;
	}

	free(wsk_do_bufora);

	for (int i = 0; i < liczba_elementow_bufora; i++) {
		printf("element numer %d = %d\n", i, wsk_do_bufora[i]);
	}

	return 0;
}


