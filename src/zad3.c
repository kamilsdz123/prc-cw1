#include <stdlib.h>
#include <stdio.h>


int main(void)
{

	//Deklaracja zmiennych

	int x=0;
	int y=100;

	printf("Zmienna x = %d \n",x);
	printf("Zmienna y = %d \n \n",y);

	//Zwiekszenie zmiennej x 20-krotnie i zwiekszenie zmiennej y 20-krotnie
	//petla for

	int i;
	for (i=1;i<=20;i++)
	{
		x=x+1;
		y=y-2;
	}

	printf("Po zmniejszeniu i zwiekszeniu przy pomocy petli for: \n");
	printf("	Zmienna x = %d \n",x);
	printf("	Zmienna y = %d \n \n",y);

	//Zwiekszenie zmiennych 20-krotnie o 2
	//petla while

	i=1;
	while(i<=20)
	{
		x=x+2;
		y=y+2;
		i++;

	}

	printf("Po zwiekszeniu przy pomocy petli while: \n");
	printf("	Zmienna x = %d \n",x);
	printf("	Zmienna y = %d \n",y);

	return EXIT_SUCCESS;
}
