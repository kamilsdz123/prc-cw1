#include <stdio.h>
#include <stdlib.h>
#include "funkcje.h"

int main(void)
{
	struct struktura struktura;
	struktura.a = 10;
	struktura.b = 5;
	struktura.wskaznik = &struktura.b;

	printf("suma 1 = %d\n", suma1(struktura));
	printf("suma 2 = %d\n", suma2(struktura));
	printf("suma 3 = %d\n", suma3(struktura));
	return 0;
}


int suma1(struct struktura str1)
{
	return (str1.a + str1.b);
}


int suma2(struct struktura str1)
{
	int y = 10;
	return (str1.a + str1.b + y);
}

int suma3(struct struktura str1)
{
	int y = 15;
	*str1.wskaznik += str1.a + y;
	return (str1.a + *str1.wskaznik);
}
