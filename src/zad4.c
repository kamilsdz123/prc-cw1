#include <stdio.h>
#include <stdlib.h>

int main (void)
{
	//Deklaracja zmiennych
	int x = 2;
	char y = 'z';
	unsigned char zmchar = 's';

	//Deklaracja wskaznikow
	int *wsk_x;
	wsk_x = &x;
	char *wsk_y;
	wsk_y = &y;
	unsigned char *wsk_zmchar;
	wsk_zmchar = &zmchar;

	//Adresy
	printf("Adresy: \n");
	printf("   int: %p \n",wsk_x);
	printf("   char: %p \n",wsk_y);
	printf("   unsigned char: %p \n",wsk_zmchar);
	printf("\n");

	//Rozmiar pamieci wskaznikow
	printf("Rozmiar pamieci wskaznikow: \n");
	printf("   int: %ld \n", sizeof(wsk_x));
	printf("   char: %ld \n", sizeof(wsk_y));
	printf("   unsigned char: %ld \n", sizeof(wsk_zmchar));
	printf("\n");

	//Rozmiary pamieci zmiennych
	printf("Rozmiar pamieci zmiennych: \n");
	printf("   int: %ld \n", sizeof(x));
	printf("   char: %ld \n", sizeof(y));
	printf("   unsigned char: %ld \n", sizeof(zmchar));
	printf("\n");

	//Wartosci zmiennych
	printf("Wartosci zmiennych: \n");
	printf("   int: %d \n", x);
	printf("   char: %c \n", y);
	printf("   unsigned char: %c \n", zmchar);
	printf("\n");


	//Zmiana wartosci
	*wsk_x = 10;
	*wsk_y = 'c';
	*wsk_zmchar = 'p';

	//Ponowny rozmiar pamieci wskaznikow
	printf("Ponowny rozmiar pamieci wskaznikow: \n");
	printf("   int: %ld \n", sizeof(wsk_x));
	printf("   char: %ld \n", sizeof(wsk_y));
	printf("   unsigned char: %ld \n", sizeof(wsk_zmchar));
	printf("\n");

		//Ponowny rozmiary pamieci zmiennych
	printf("Ponowny rozmiar pamieci zmiennych: \n");
	printf("   int: %ld \n", sizeof(x));
	printf("   char: %ld \n", sizeof(y));
	printf("   unsigned char: %ld \n", sizeof(zmchar));
	printf("\n");

		//Ponowne wartosci zmiennych
	printf("Ponowne wartosci zmiennych: \n");
	printf("   int: %d \n", x);
	printf("   char: %c \n", y);
	printf("   unsigned char: %c \n", zmchar);
	printf("\n");










}
